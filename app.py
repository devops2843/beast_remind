#7.3
from flask import Flask, render_template

app = Flask(__name__, template_folder='./')
@app.route('/')

def home():
    return render_template('beasts.html')


if __name__ == '__main__':
    app.run(debug=True)
